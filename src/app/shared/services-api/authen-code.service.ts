import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenCodeService {
  token: any;
  httpOptions: any;
  httpOptions2: any;

  constructor(
    @Inject('API_URL') private apiUrl: string,
    @Inject('API_TOKEN') private accessToken: string,

    private _httpClient: HttpClient,
  ) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    this.httpOptions2 = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accessToken
      })
    };

  }

  latestAuthenCode(cid: any) {    
    const _url = `http://localhost:8189/api/nhso-service/latest-authen-code/${cid}`;
    return this._httpClient.get(_url, this.httpOptions).toPromise();
  }

  saveAuthenaCode(info: any) {
    const _url = `${this.apiUrl}/claimdmht/saveauthencode`;
    return this._httpClient.post(_url, info, this.httpOptions2).toPromise();
  }



}