import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClaimdmhtService {

  token: any;
  httpOptions: any;

  constructor(
    @Inject('API_URL') private apiUrl: string,
    @Inject('API_TOKEN') private accessToken: string,
    private _httpClient: HttpClient,
  ) {
    this.token = sessionStorage.getItem('accessToken');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accessToken
      })
    };
  }

  list(info: any) {
    const _url = `${this.apiUrl}/claimdmht/list`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  save(info: any) {
    const _url = `${this.apiUrl}/claimdmht`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

}