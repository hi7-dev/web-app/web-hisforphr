import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthenTokenService {

  token: any;
  httpOptions: any;

  constructor(
    @Inject('API_URL') private apiUrl: string,
    @Inject('API_TOKEN') private accessToken: string,
    private _httpClient: HttpClient,
  ) {
    this.token = sessionStorage.getItem('accessToken');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + this.accessToken
      })
    };
  }

  listToken(info: any) {
    // console.log(info);
    
    const _url = `${this.apiUrl}/moph/getToken?user=${info.username}&password_hash=${info.password}&hospital_code=${info.hospcode}`;
    // console.log(_url);
    
    return this._httpClient.get(_url, this.httpOptions).toPromise();
  }

  listTokenPassward(info: any) {
    // console.log(info);
    
    const _url = `${this.apiUrl}/moph/getTokenPassword?username=${info.username}&password=${info.password}&hospcode=${info.hospcode}`;
    // console.log(_url);
    
    return this._httpClient.get(_url, this.httpOptions).toPromise();
  }

}