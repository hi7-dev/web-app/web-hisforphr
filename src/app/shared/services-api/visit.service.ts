import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VisitService {

  token: any;
  httpOptions: any;

  constructor(
    @Inject('API_URL') private apiUrl: string,
    @Inject('API_TOKEN') private accessToken: string,
    private _httpClient: HttpClient,
  ) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accessToken
      })
    };
  }

  list_phr(info: any) {
    const _url = `${this.apiUrl}/visit/info_visit`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  list_clain(info: any) {
    const _url = `${this.apiUrl}/claimdmht/getClaimVisit`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  list_authencode(info: any) {
    const _url = `${this.apiUrl}/visit/info_authen_code`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }
  list_not_authencode(info: any) {
    const _url = `${this.apiUrl}/visit/info_not_authen_code`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  insertPhr(info: any){
    const _url = `${this.apiUrl}/phr/export`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  insertClaim(info: any){
    const _url = `${this.apiUrl}/claimdmht/export`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  list_clain_epi(info: any) {
    const _url = `${this.apiUrl}/claimdmht/getClaimEpiVisit`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  list_clain_dt(info: any) {
    const _url = `${this.apiUrl}/claimdmht/getClaimDtVisit`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  insertClaimEpi(info: any){
    const _url = `${this.apiUrl}/claimdmht/exportEpi`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  insertClaimDt(info: any){
    const _url = `${this.apiUrl}/claimdmht/exportDt`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  list_pregnancytest(info: any) {
    const _url = `${this.apiUrl}/visit/info_pregnancy_test`;
    return this._httpClient.post(_url, info, this.httpOptions).toPromise();
  }

}