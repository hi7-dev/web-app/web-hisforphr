import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    ClarityModule,
    HttpClientModule,

  ],
  exports:[
    FormsModule,
    BrowserAnimationsModule,
    ClarityModule,
    HttpClientModule,
  ]
  
})
export class SharedModule { }
