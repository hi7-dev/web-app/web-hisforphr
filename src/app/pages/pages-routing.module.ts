import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ViewPhrComponent } from './view-phr/view-phr.component';
import { ViewClaimComponent } from './view-claim/view-claim.component';
import { RepClaimComponent } from './rep-claim/rep-claim.component';
import { ViewAuthencodeComponent } from './view-authencode/view-authencode.component';
import { ViewAuthentokenComponent } from './view-authentoken/view-authentoken.component';
import { ViewClaimEpiComponent } from './view-claim-epi/view-claim-epi.component';
import { ViewClaimDtComponent } from './view-claim-dt/view-claim-dt.component';
import { ViewAutoAuthenComponent } from './view-auto-authen/view-auto-authen.component';
import { ViewPregnancytestComponent } from './view-pregnancytest/view-pregnancytest.component';

const routes: Routes = [
  {
    path: 'pages', component: PagesComponent,
    children: [
      { path: '', redirectTo: 'viewtoken', pathMatch: 'full' },
      { path: 'viewphr', component: ViewPhrComponent },
      { path: 'viewclaim', component: ViewClaimComponent },
      { path: 'repclaim', component: RepClaimComponent },
      { path: 'authencode', component: ViewAuthencodeComponent },
      { path: 'viewtoken', component: ViewAuthentokenComponent },
      { path: 'viewclaimepi', component: ViewClaimEpiComponent },
      { path: 'viewclaimdt', component: ViewClaimDtComponent },
      { path: 'loadauthen', component: ViewAutoAuthenComponent },
      { path: 'pregnancytest', component: ViewPregnancytestComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
