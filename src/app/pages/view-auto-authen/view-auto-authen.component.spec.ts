import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAutoAuthenComponent } from './view-auto-authen.component';

describe('ViewAutoAuthenComponent', () => {
  let component: ViewAutoAuthenComponent;
  let fixture: ComponentFixture<ViewAutoAuthenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAutoAuthenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewAutoAuthenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
