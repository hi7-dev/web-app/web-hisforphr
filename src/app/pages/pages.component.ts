import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  token: any;
  constructor() { }

  ngOnInit(): void {
    this.token = sessionStorage.getItem('authentoken');

    // sessionStorage.removeItem('token')
  }
  logout(){
    sessionStorage.removeItem('authentoken');    
    location.reload(); 
  }

}
