import { Component, OnInit } from '@angular/core';
import { VisitService } from '../../shared/services-api/visit.service'
import { AlertService } from '../../shared/alert.service'
import * as moment from 'moment-timezone';
import { Router } from '@angular/router';

@Component({
  selector: 'app-views',
  templateUrl: './views.component.html',
  styleUrls: ['./views.component.scss']
})
export class ViewsComponent implements OnInit {
  toDate:any = Date();
  dataStart:any = moment(this.toDate).format('YYYY-MM-DD');
  dateEnd:any = moment(this.toDate).format('YYYY-MM-DD');
  viewItem:any = [];

  constructor(
    private visitService:VisitService,
    private alertService:AlertService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getInfo();
    if(!sessionStorage.getItem('authentoken')){
      this.router.navigate(["pages/viewtoken"]);
    }

  }

  async getInfo(){
    let info:any = {
      "dateStart":moment(this.dataStart).format('YYYY-MM-DD'),
      "dateEnd":moment(this.dateEnd).format('YYYY-MM-DD')
  }
    try {
      let rs:any = await this.visitService.list_phr(info);
      console.log(rs);
      if(rs.statusCode == 200){
 
        this.viewItem = rs.results;

      }else{
        this.alertService.error(rs.text,'เกิดข้อผิดพลาด');
      }
    } catch (error:any) {
      console.log(error.error);
      this.alertService.error(error.error,'เกิดข้อผิดพลาด');
    }
  }
}
