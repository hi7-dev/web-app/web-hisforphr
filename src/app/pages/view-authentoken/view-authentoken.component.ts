import { Component, OnInit } from '@angular/core';
import { AuthenTokenService } from '../../shared/services-api/authen-token.service'
import { AlertService } from '../../shared/alert.service'
import { Router } from '@angular/router';

// import * as moment from 'moment-timezone';
@Component({
  selector: 'app-view-authentoken',
  templateUrl: './view-authentoken.component.html',
  styleUrls: ['./view-authentoken.component.scss']
})
export class ViewAuthentokenComponent implements OnInit {
  toDate:any = Date();
  username:any;
  password:any;  
  hospcode:any;  
  token:any;  

  viewItem:any = [];
  loading:boolean = false;
  loadingTable:boolean =false;

  constructor(
    private authenTokenService:AuthenTokenService,
    private alertService:AlertService,
    private router: Router,

  ) { }

  ngOnInit(): void {

    // this.getInfo();
    if(sessionStorage.getItem('authentoken') != '[object Object]'){
      // this.router.navigate(["pages/viewtoken"]);
      this.token = sessionStorage.getItem('authentoken');
    }else{
      sessionStorage.removeItem('authentoken');
      // this.router.navigate(["pages/viewclaim"]);

    }
  }
  async getSearcha(){
    sessionStorage.setItem('username',this.username);
    sessionStorage.setItem('password',this.password);
    sessionStorage.setItem('hospcode',this.hospcode);
    this.getInfo();
  }

  async getInfo(){
    this.loadingTable = true
    let info:any = {
      "username":this.username,
      "password":this.password,
      "hospcode":this.hospcode
  }
    try {
      let rs:any = await this.authenTokenService.listTokenPassward(info);
      console.log(rs);
      if(rs.statusCode == 200){
 
        // this.viewItem = rs;

        sessionStorage.setItem('authentoken',rs.info);
        this.token = sessionStorage.getItem('authentoken');
        this.loadingTable = false
        location.reload();

      }else{
        this.loadingTable = false

        this.alertService.error(rs.text,'เกิดข้อผิดพลาด');
      }
    } catch (error:any) {
      console.log(error.error);
      this.loadingTable = false

      this.alertService.error(error.error.results.text,'เกิดข้อผิดพลาด');
    }
  }


}
