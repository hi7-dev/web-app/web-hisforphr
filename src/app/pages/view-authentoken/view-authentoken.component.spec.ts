import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAuthentokenComponent } from './view-authentoken.component';

describe('ViewAuthentokenComponent', () => {
  let component: ViewAuthentokenComponent;
  let fixture: ComponentFixture<ViewAuthentokenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAuthentokenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewAuthentokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
