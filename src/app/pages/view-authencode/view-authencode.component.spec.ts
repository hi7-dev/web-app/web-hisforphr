import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAuthencodeComponent } from './view-authencode.component';

describe('ViewAuthencodeComponent', () => {
  let component: ViewAuthencodeComponent;
  let fixture: ComponentFixture<ViewAuthencodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAuthencodeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewAuthencodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
