import { Component, OnInit } from '@angular/core';
import { VisitService } from '../../shared/services-api/visit.service'
import { PhrService } from '../../shared/services-api/phr.service'
import { AuthenCodeService } from '../../shared/services-api/authen-code.service'
import { Router } from '@angular/router';

import { AlertService } from '../../shared/alert.service'
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-view-authencode',
  templateUrl: './view-authencode.component.html',
  styleUrls: ['./view-authencode.component.scss']
})
export class ViewAuthencodeComponent implements OnInit {
  toDate:any = Date();
  dataStart:any;
  dateEnd:any;
  viewItem:any = [];

  loading:boolean = false;
  loadingTable:boolean =false;

  constructor(
    private authenCodeService:AuthenCodeService,
    private phrService:PhrService,
    private visitService:VisitService,
    private alertService:AlertService,
    private router: Router,

  ) { 
    if(!sessionStorage.getItem('dataStart') && !sessionStorage.getItem('dateEnd')){
      sessionStorage.setItem('dataStart',moment(this.toDate).format('YYYY-MM-DD'));
      sessionStorage.setItem('dateEnd',moment(this.toDate).format('YYYY-MM-DD'));  
    }else{
      this.dataStart = sessionStorage.getItem('dataStart');
      this.dateEnd = sessionStorage.getItem('dateEnd');
    }
  }

  ngOnInit(): void {
    this.getInfo();
    if(!sessionStorage.getItem('authentoken')){      
      this.router.navigate(["pages/viewtoken"]);
    }
  }

  async getSearcha(){
    sessionStorage.setItem('dataStart',this.dataStart);
    sessionStorage.setItem('dateEnd',this.dateEnd);

    this.getInfo();
  }

  async getCancel(){
    this.dataStart = moment(this.toDate).format('YYYY-MM-DD');
    this.dateEnd = moment(this.toDate).format('YYYY-MM-DD');
    sessionStorage.setItem('dataStart',this.dataStart);
    sessionStorage.setItem('dateEnd',this.dateEnd);  

    this.getInfo();
  }

  async getInfo(){
    this.loading = true;

    const delayInterval = 6000; // set delay interval 200 milisecond 200000

    let info:any = {
      "dateStart":sessionStorage.getItem('dataStart'),
      "dateEnd":sessionStorage.getItem('dateEnd')
  }
    try {
      let rs:any = await this.visitService.list_authencode(info);
      // console.log(rs);
      console.log('vist total :',rs.results.length);
      if(rs.statusCode == 200){
 
        this.viewItem = await rs.results;

        let rs_not: any = await this.visitService.list_not_authencode(info);
        // console.log(rs_not);
        console.log('vist code null :',rs_not.results.length);

        if (rs_not.results.length > 0) {

          for (let x of rs_not.results) {

            let rs_auth: any = await this.authenCodeService.latestAuthenCode(x.cid);

            // console.log(rs_auth);
            let regist_date:any = moment(rs_auth.claimDateTime).format('YYYY-MM-DD');
            // let regist_date_:any = moment(this.toDate).format('YYYY-MM-DD');
            let regist_time:any = moment(rs_auth.claimDateTime).format('HH-mm-ss');

            if(rs_auth.claimCode && rs_auth.claimType && !x.claimCode && regist_date == this.dateEnd){
              console.log('claimCode : ',rs_auth.claimCode);
              
              let info:any = {
                cid:x.cid,
                vn:x.vn,
                json_data:{"pid":x.cid},
                claimCode:rs_auth.claimCode,
                claimType:rs_auth.claimType,
                cln:rs_auth.cln,
                regist_date:regist_date,
                regist_time:regist_time
              }
              // console.log(info);
              let seve_auth:any = await this.authenCodeService.saveAuthenaCode(info);
              // console.log(seve_auth);
            }
            await new Promise(resolve => setTimeout(resolve, delayInterval));
          }

        }

        await this.getInfoLoad();
        this.loading = false;


      }else{
        this.alertService.error(rs.text,'เกิดข้อผิดพลาด');
        this.loading = false;

      }
    } catch (error:any) {
      console.log(error.error);
      this.alertService.error(error.error.results.text,'เกิดข้อผิดพลาด');
      this.loading = false;

    }
  }

  async getLoad(x:any) {
    console.log(x);
    this.loading = true;


    let rs_auth: any = await this.authenCodeService.latestAuthenCode(x.cid);

    console.log(rs_auth);
    let regist_date:any = moment(rs_auth.claimDateTime).format('YYYY-MM-DD');
    // let regist_date_:any = moment(this.toDate).format('YYYY-MM-DD');
    let regist_time:any = moment(rs_auth.claimDateTime).format('HH-mm-ss');

    if(rs_auth.claimCode && rs_auth.claimType && !x.claimCode && regist_date == this.dateEnd){
      console.log('claimCode : ',rs_auth.claimCode);
      
      let info:any = {
        cid:x.cid,
        vn:x.vn,
        json_data:{"pid":x.cid},
        claimCode:rs_auth.claimCode,
        claimType:rs_auth.claimType,
        cln:rs_auth.cln,
        regist_date:regist_date,
        regist_time:regist_time
      }
      // console.log(info);
      let seve_auth:any = await this.authenCodeService.saveAuthenaCode(info);
      // console.log(seve_auth);
      await this.getInfoLoad();
      this.loading = false;


    }
  }

  async getInfoLoad(){
    this.viewItem = [];
    let info:any = {
      "dateStart":sessionStorage.getItem('dataStart'),
      "dateEnd":sessionStorage.getItem('dateEnd')
  }
    try {
      let rs:any = await this.visitService.list_authencode(info);
      // console.log(rs);
      if(rs.statusCode == 200){
 
        this.viewItem = await rs.results;

      }else{
        this.alertService.error(rs.text,'เกิดข้อผิดพลาด');
      }
    } catch (error:any) {
      console.log(error.error);
      this.alertService.error(error.error.results.text,'เกิดข้อผิดพลาด');
    }
  }

  async exportClaimAll() {
    const delayInterval = 6000; // set delay interval 200 milisecond 200000
    this.loading = true;
    let info:any = {
      "dateStart":sessionStorage.getItem('dataStart'),
      "dateEnd":sessionStorage.getItem('dateEnd')
  }
  let rs: any = await this.visitService.list_authencode(info);
  console.log('vist total :',rs.results.length);
  if (rs.statusCode == 200) {

    this.viewItem = await rs.results;

    let rs_not: any = await this.visitService.list_not_authencode(info);
    console.log('vist code null :',rs_not.results.length);

    if (rs_not.results.length > 0) {

      for (let x of rs_not.results) {

          let rs_auth: any = await this.authenCodeService.latestAuthenCode(x.cid);

          // console.log(x.vn);
          if (rs_auth) {
            let regist_date: any = moment(rs_auth.claimDateTime).format('YYYY-MM-DD');
            // let regist_date_:any = moment(this.toDate).format('YYYY-MM-DD');
            let regist_time: any = moment(rs_auth.claimDateTime).format('HH-mm-ss');

            if (rs_auth.claimCode && rs_auth.claimType && !x.claimCode && regist_date == this.dateEnd) {
              console.log('timeout after 2.5 seconds || vn :',x.vn);
              console.log('claimCode : ', rs_auth.claimCode);

              let info: any = {
                cid: x.cid,
                vn: x.vn,
                json_data: { "pid": x.cid },
                claimCode: rs_auth.claimCode,
                claimType: rs_auth.claimType,
                cln: rs_auth.cln,
                regist_date: regist_date,
                regist_time: regist_time
              }
              // console.log(info);
              let seve_auth: any = await this.authenCodeService.saveAuthenaCode(info);
              // console.log(seve_auth);
            }
          }
          await new Promise(resolve => setTimeout(resolve, delayInterval));
      }
    }
    await this.getInfoLoad();
    this.loading = false;

  } else {
    this.alertService.error(rs.text, 'เกิดข้อผิดพลาด');
    this.loading = false;

  }
}

}
