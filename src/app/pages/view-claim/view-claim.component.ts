import { Component, OnInit } from '@angular/core';
import { VisitService } from '../../shared/services-api/visit.service'
import { ClaimdmhtService } from '../../shared/services-api/claimdmht.service'
import { AlertService } from '../../shared/alert.service'
import * as moment from 'moment-timezone';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-claim',
  templateUrl: './view-claim.component.html',
  styleUrls: ['./view-claim.component.scss']
})
export class ViewClaimComponent implements OnInit {
  toDate: any = Date();
  dataStart: any;
  dateEnd: any;
  viewItem: any = [];
  loading: boolean = false;
  loadingTable: boolean = false;

  constructor(
    private visitService: VisitService,
    private claimdmhtService: ClaimdmhtService,
    private alertService: AlertService,
    private router: Router,
  ) {
    if (!sessionStorage.getItem('dataStart') && !sessionStorage.getItem('dateEnd')) {
      sessionStorage.setItem('dataStart', moment(this.toDate).format('YYYY-MM-DD'));
      sessionStorage.setItem('dateEnd', moment(this.toDate).format('YYYY-MM-DD'));
    } else {
      this.dataStart = sessionStorage.getItem('dataStart');
      this.dateEnd = sessionStorage.getItem('dateEnd');
    }
  }

  ngOnInit(): void {
    this.getInfo();
    if(!sessionStorage.getItem('authentoken')){
      this.router.navigate(["pages/viewtoken"]);
    }
    // if(!sessionStorage.getItem('dataStart') && !sessionStorage.getItem('dateEnd')){
    //   sessionStorage.setItem('dataStart',this.dataStart);
    //   sessionStorage.setItem('dateEnd',this.dateEnd);  
    // }
  }

  async getSearcha() {
    sessionStorage.setItem('dataStart', this.dataStart);
    sessionStorage.setItem('dateEnd', this.dateEnd);

    this.getInfo();
  }

  async getCancel() {
    this.dataStart = moment(this.toDate).format('YYYY-MM-DD');
    this.dateEnd = moment(this.toDate).format('YYYY-MM-DD');
    sessionStorage.setItem('dataStart', this.dataStart);
    sessionStorage.setItem('dateEnd', this.dateEnd);

    this.getInfo();
  }

  async getInfo() {
    this.loadingTable = true
    let info: any = {
      "dateStart": sessionStorage.getItem('dataStart'),
      "dateEnd": sessionStorage.getItem('dateEnd')
    }
    try {
      let rs: any = await this.visitService.list_clain(info);
      // console.log(rs);
      if (rs.statusCode == 200) {

        this.viewItem = rs.results;
        this.loadingTable = false

      } else {
        this.loadingTable = false

        this.alertService.error(rs.text, 'เกิดข้อผิดพลาด');
      }
    } catch (error: any) {
      console.log(error.error);
      this.loadingTable = false

      this.alertService.error(error.error.results.text, 'เกิดข้อผิดพลาด');
    }
  }

  async alertCliam(i: any) {
    let confirm: any = await this.alertService.confirm();
    if (confirm) {
      this.exportCliam(i);
    }
  }

  async exportCliam(i: any) {
    this.loading = true
    let info = {
      "hn": i.hn,
      "vn": i.vn,
      "is_used_dm": i.is_used_dm,
      "is_used_ht": i.is_used_ht,
      "token": sessionStorage.getItem('authentoken')
    }
    let status_dmht: any;
    if (i.is_used_dm == 1) {
      status_dmht = 'dm'
    } else {
      status_dmht = 'ht'

    }

    try {
      let rs: any = await this.visitService.insertClaim(info);
      // console.log(rs);
      if (rs.statusCode == 200) {

        if (rs.results_claim.status == 200) {
          let rs_data: any = await {
            "code_status": rs.results_claim.status,
            "message": rs.results_claim.message,
            "message_th": rs.results_claim.message_th,
            "seq": rs.results.seq,
            "hn": rs.results.hn,
            "transaction_uid": rs.results_claim.data.transaction_uid,
            "status_dmht": status_dmht,
            "verify_url": rs.results_claim.data.verify_url
          }
          await this.saveCliamRep(rs_data);
          this.loading = false
          this.alertService.success(rs.results_claim.message_th);

        } else {
          let rs_data: any = await {
            "code_status": rs.results_claim.status,
            "message": rs.results_claim.message,
            "message_th": rs.results_claim.message_th,
            "seq": rs.results.seq,
            "hn": rs.results.hn,
            "status_dmht": status_dmht
          }
          await this.saveCliamRep(rs_data);
          this.loading = false
          this.alertService.error(rs.results_claim.message_th, 'เกิดข้อผิดพลาด');

        }
      }

    } catch (error: any) {
      console.log(error.error);
      this.loading = false
      this.alertService.error(error.error.results.text, 'เกิดข้อผิดพลาด');
    }
  }

  async saveCliamRep(i: any) {
    let info = await {
      "code_status": i.code_status,
      "message": i.message,
      "message_th": i.message_th,
      "seq": i.seq,
      "hn": i.hn,
      "transaction_uid": i.transaction_uid,
      "verify_url": i.verify_url,
      "status_dmht": i.status_dmht,
      "d_update": moment(this.toDate).format('YYYY-MM-DD')
    }
    // console.log(info);

    try {
      let rs: any = await this.claimdmhtService.save(info);
      // console.log(rs);
      if (rs.statusCode) {
        this.getInfo();

      }
    } catch (error: any) {
      console.log(error.error);
      // this.alertService.error(error.error,'เกิดข้อผิดพลาด');
    }
  }

  async exportClaimAll() {
    const delayInterval = 200; // 200 milisecond
    this.loading = true
    let data = this.viewItem;
    // console.log(data);

    //  loop data for export claim
    for (let v of data) {
      let info = {
        "hn": v.hn,
        "vn": v.vn,
        "is_used_dm": v.is_used_dm,
        "is_used_ht": v.is_used_ht,
        "token": sessionStorage.getItem('authentoken')
      }

      let status_dmht: any;
      if (v.is_used_dm == 1) {
        status_dmht = 'dm'
      } else {
        status_dmht = 'ht'

      }

      // check status send phr
      if (v.code_status != '200') {
        // send phr per vn
        try {
          let rs: any = await this.visitService.insertClaim(info);
          // console.log(rs);

          // result data status 200 send phr
          if (rs.statusCode == 200) {

            if (rs.results_claim.status == 200) {
              let rs_data: any = await {
                "code_status": rs.results_claim.status,
                "message": rs.results_claim.message,
                "message_th": rs.results_claim.message_th,
                "seq": rs.results.seq,
                "hn": rs.results.hn,
                "transaction_uid": rs.results_claim.data.transaction_uid,
                "status_dmht": status_dmht,
                "verify_url": rs.results_claim.data.verify_url
              }
              await this.saveCliamRep(rs_data);
              // this.loading = false
              // this.alertService.success(rs.results_phr.Message);
              console.log('success : { vn: ' + info.vn + ' hn: ' + info.hn + '}');
            } else {
              let rs_data: any = await {
                "code_status": rs.results_claim.status,
                "message": rs.results_claim.message,
                "message_th": rs.results_claim.message_th,
                "seq": rs.results.seq,
                "hn": rs.results.hn,
                "status_dmht": status_dmht
              }
              await this.saveCliamRep(rs_data);
              // this.loading = false
              // this.alertService.error(rs.results_phr.Message, 'เกิดข้อผิดพลาด');
              console.log('error : { vn: ' + info.vn + ' hn: ' + info.hn + '}');
            }
          }
        } catch (error: any) {
          // console.log(error.error);
          this.alertService.error(error.error.results.text, 'เกิดข้อผิดพลาด');
        }
        await new Promise(resolve => setTimeout(resolve, delayInterval)); // wait 200 milisecond
      }
    }
  }

}
