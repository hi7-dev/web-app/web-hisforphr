import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';

import { ViewsComponent } from './views/views.component';
import { ViewPhrComponent } from './view-phr/view-phr.component';
import { ViewClaimComponent } from './view-claim/view-claim.component';
import { RepClaimComponent } from './rep-claim/rep-claim.component';
import { ViewAuthencodeComponent } from './view-authencode/view-authencode.component';
import { ViewAuthentokenComponent } from './view-authentoken/view-authentoken.component';
import { ViewClaimEpiComponent } from './view-claim-epi/view-claim-epi.component';
import { ViewClaimDtComponent } from './view-claim-dt/view-claim-dt.component';
import { ViewAutoAuthenComponent } from './view-auto-authen/view-auto-authen.component';
import { ViewPregnancytestComponent } from './view-pregnancytest/view-pregnancytest.component';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeTh from '@angular/common/locales/th';

registerLocaleData(localeTh);

@NgModule({
  declarations: [
    PagesComponent,
    ViewsComponent,
    ViewPhrComponent,
    ViewClaimComponent,
    RepClaimComponent,
    ViewAuthencodeComponent,
    ViewAuthentokenComponent,
    ViewClaimEpiComponent,
    ViewClaimDtComponent,
    ViewAutoAuthenComponent,
    ViewPregnancytestComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    PagesRoutingModule,
  ],
  providers: [{provide: LOCALE_ID, useValue: 'th-TH'}]
})
export class PagesModule { }
