import { Component, OnInit } from '@angular/core';
import { VisitService } from '../../shared/services-api/visit.service'
import { ClaimdmhtService } from '../../shared/services-api/claimdmht.service'
import { AlertService } from '../../shared/alert.service'
import * as moment from 'moment-timezone';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rep-claim',
  templateUrl: './rep-claim.component.html',
  styleUrls: ['./rep-claim.component.scss']
})
export class RepClaimComponent implements OnInit {
  toDate:any = Date();
  dataStart:any = moment(this.toDate).format('YYYY-MM-DD');
  dateEnd:any = moment(this.toDate).format('YYYY-MM-DD');
  viewItem:any = [];

  constructor(
    private claimdmhtService:ClaimdmhtService,
    private visitService:VisitService,
    private alertService:AlertService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getInfo();
    if(!sessionStorage.getItem('authentoken')){
      this.router.navigate(["pages/viewtoken"]);
    }
  }

  async getSearcha(){
    this.getInfo();
  }

  async getCancel(){
    this.dataStart = moment(this.toDate).format('YYYY-MM-DD');
    this.dateEnd = moment(this.toDate).format('YYYY-MM-DD');
    this.getInfo();
  }

  async getInfo(){
    let info:any = {
      "limit": 1000,
      "offset": 0,
      "orderby":"",
      "d_update_start":moment(this.dataStart).format('YYYY-MM-DD'),
      "d_update_end":moment(this.dateEnd).format('YYYY-MM-DD')
  }
    try {
      let rs:any = await this.claimdmhtService.list(info);
      console.log(rs);
      if(rs.statusCode == 200){
 
        this.viewItem = rs.results;

      }else{
        this.alertService.error(rs.text,'เกิดข้อผิดพลาด');
      }
    } catch (error:any) {
      console.log(error.error);
      this.alertService.error(error.error.results.text,'เกิดข้อผิดพลาด');
    }
  }
}
