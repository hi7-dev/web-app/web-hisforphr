import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepClaimComponent } from './rep-claim.component';

describe('RepClaimComponent', () => {
  let component: RepClaimComponent;
  let fixture: ComponentFixture<RepClaimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepClaimComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
