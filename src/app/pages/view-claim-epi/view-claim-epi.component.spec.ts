import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewClaimEpiComponent } from './view-claim-epi.component';

describe('ViewClaimEpiComponent', () => {
  let component: ViewClaimEpiComponent;
  let fixture: ComponentFixture<ViewClaimEpiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewClaimEpiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewClaimEpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
