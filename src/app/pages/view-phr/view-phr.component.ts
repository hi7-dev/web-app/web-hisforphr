import { Component, OnInit } from '@angular/core';
import { VisitService } from '../../shared/services-api/visit.service'
import { PhrService } from '../../shared/services-api/phr.service'
import { AuthenTokenService } from '../../shared/services-api/authen-token.service'
import { Router } from '@angular/router';
import { AlertService } from '../../shared/alert.service'
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-view-phr',
  templateUrl: './view-phr.component.html',
  styleUrls: ['./view-phr.component.scss'],

})
export class ViewPhrComponent implements OnInit {
  toDate: any = Date();
  dataStart: any;
  dateEnd: any;
  viewItem: any = [];

  username: any;
  password: any;
  hospcode: any;
  token: any;
  is_display_all: boolean = false;

  loading: boolean = false;
  loadingTable: boolean = false;

  constructor(
    private phrService: PhrService,
    private visitService: VisitService,
    private alertService: AlertService,
    private authenTokenService: AuthenTokenService,
    private router: Router,
  ) {
    if (!sessionStorage.getItem('dataStart') && !sessionStorage.getItem('dateEnd')) {
      sessionStorage.setItem('dataStart', moment(this.toDate).format('YYYY-MM-DD'));
      sessionStorage.setItem('dateEnd', moment(this.toDate).format('YYYY-MM-DD'));
    } else {
      this.dataStart = sessionStorage.getItem('dataStart');
      this.dateEnd = sessionStorage.getItem('dateEnd');
    }
    this.username = sessionStorage.getItem('username');
    this.password = sessionStorage.getItem('password');
    this.hospcode = sessionStorage.getItem('hospcode');
  }

  ngOnInit(): void {
    this.getInfo();
    if (!sessionStorage.getItem('authentoken')) {
      this.router.navigate(["pages/viewtoken"]);
    }
  }

  async getSearcha() {
    sessionStorage.setItem('dataStart', this.dataStart);
    sessionStorage.setItem('dateEnd', this.dateEnd);

    this.getInfo();
  }

  async getCancel() {
    this.dataStart = moment(this.toDate).format('YYYY-MM-DD');
    this.dateEnd = moment(this.toDate).format('YYYY-MM-DD');
    sessionStorage.setItem('dataStart', this.dataStart);
    sessionStorage.setItem('dateEnd', this.dateEnd);

    this.getInfo();
  }

  async getInfo() {
    let info: any = {
      "dateStart": sessionStorage.getItem('dataStart'),
      "dateEnd": sessionStorage.getItem('dateEnd')
    }
    try {
      let rs: any = await this.visitService.list_phr(info);
      // console.log(rs);
      if (rs.statusCode == 200) {

        this.viewItem = rs.results;
        if (this.is_display_all) {
          this.viewItem = rs.results;
        } else {
          // status ont in 200 and 500
          this.viewItem = rs.results.filter((item: any) => item.code_status != '200' && item.code_status != '500');
        }
      } else {
        this.alertService.error(rs.text, 'ไม่สามารถค้นหารายการมารับบริการได้');
      }
    } catch (error: any) {
      console.log(error.error);
      this.alertService.error(error.error.results.text, 'Server Error !!');
    }
  }


  async alertPhr(i: any) {
    let confirm: any = await this.alertService.confirm();
    if (confirm) {
      this.exportPhr(i);
    }
  }

  async exportPhr(i: any) {
    this.loading = true
    let info = {
      "hn": i.hn,
      "vn": i.vn,
      "token": sessionStorage.getItem('authentoken')
    }
    // send phr per vn
    let rs: any = await this.sendPhr(info);
    console.log(rs);
    if(rs.results_phr.MessageCode === 200) {
      this.loading = false;
      this.getInfo();
      this.alertService.success('การส่งข้อมูลสำเร็จ');
    } else {
      this.loading = false;
      this.alertService.error('การส่งข้อมูลไม่สำเร็จ ' + rs.results_phr.Message);
    }

  }

  async exportPhrAll() {

    this.loading = true;

    const delayInterval = 10000; // set delay interval 10000 milisecond
    const wait2minute = 120000; // set delay interval 120000 milisecond
    const wait5minute = 300000; // set delay interval 300000 milisecond

    let total_success = 0;
    let total_error = 0;

    // set data to send is not status 200 or 500
    let sendData: any = this.viewItem.filter((item: any) => item.code_status != '200' && item.code_status != '500');

    let total = sendData.length;

    if (sendData.length > 0) { // check has data to send
      // loop send phr
      for (let i = 0; i <= sendData.length; i++) {

        // set info data
        let info = {
          "hn": sendData[i].hn,
          "vn": sendData[i].vn,
          "token": sessionStorage.getItem('authentoken')
        }

        // send phr per vn
        let rs: any = await this.sendPhr(info);

        // if error 
        if (rs.results_phr.MessageCode === 500 && rs.results_phr.Message.includes('Too many parallel processing')) { // error too many parallel processing
          await new Promise(resolve => setTimeout(resolve, wait5minute)); // wait 300000 milisecond
          i = i - 1; // set i - 1 reindex to resend
          total_error += 1;
          // console.log('too many parallel processing');
        } else if (rs.results_phr.MessageCode == 401) { // error token expire or invalid
          // console.log('token expire or invalid');
          i = i - 1; // set i - 1 reindex to resend
          await this.regenerateToken(); // regenerate token
          await new Promise(resolve => setTimeout(resolve, delayInterval)); // wait 10000 milisecond
        } else if (rs.results_phr.MessageCode == 500) { // error data incomplete
          // console.log('data incomplete');
          total_error += 1;
          await new Promise(resolve => setTimeout(resolve, delayInterval)); // wait 10000 milisecond
        } else if (rs.results_phr.MessageCode == 502 || rs.results_phr.MessageCode == 504 ) { // error response data not found
          // console.log('response data not found');
          total_error += 1;
          await new Promise(resolve => setTimeout(resolve, wait2minute)); // wait 120000 milisecond
        } else { // success
          total_success += 1;
          await new Promise(resolve => setTimeout(resolve, delayInterval)); // wait 10000 milisecond
        }
      }
    }

    this.loading = false;
    this.getInfo();
    this.alertService.success('การส่งข้อมูลเสร็จสิ้น ส่งข้อมูลสำเร็จ: ' + total_success + ' รายการ, ส่งข้อมูลไม่สำเร็จ: ' + total_error + ' รายการ, รวมทั้งหมด: ' + total + ' รายการ');

  }
  // method send to phr 
  async sendPhr(info: any) {

    let res: any = {
      "statusCode": 500,
      "results_phr": {
        "MessageCode": 502,
        "Message": 'Response data not found'
      }
    }; // response data

    try {
      // send data
      res = await this.visitService.insertPhr(info);

      // result data status 200 send phr
      if (res.statusCode == 200) {
        // check res.results_phr is string and content have 504
        if (typeof res.results_phr === 'string') {
          if (res.results_phr.includes('504')) {
            res.results_phr = {
              "MessageCode": 504,
              "Message": 'Gateway Time-out'
            } // set message 504 Gateway Time-out
          } else {
            let results: any = res.results_phr;
            res.results_phr = {
              "MessageCode": 502,
              "Message": results
            } // set message 504 Gateway Time-out
          }
        }
        // set log data
        let rs_data: any = await {
          "MessageCode": res.results_phr.MessageCode || '502',
          "Message": res.results_phr.Message || 'Response data not found',
          "vn": info.vn,
          "hn": info.hn
        }
        // save log data
        await this.saveCliamRep(rs_data);
      }
    } catch (error: any) {
      this.alertService.error(error, 'เกิดข้อผิดผลาดในการส่งข้อมูล VN: ' + info.vn);
    }
    return res;
  }

  async saveCliamRep(i: any) {
    let info = await {
      "MessageCode": i.MessageCode,
      "Message": i.Message,
      "vn": i.vn,
      "hn": i.hn,
      "create_date": moment(this.toDate).format('YYYY-MM-DD'),
      "create_time": moment(this.toDate).format('HH:mm:ss')
    }
    // console.log(info);

    try {
      // save log data
      await this.phrService.save(info);
    } catch (error: any) {
      // console.log(error);
      this.alertService.error(error, 'เกิดข้อผิดผลาดในการบันทึกข้อมูลตอบกลับ VN: ' + info.vn);
    }
  }

  async regenerateToken() {
    let info: any = {
      "username": this.username,
      "password": this.password,
      "hospcode": this.hospcode
    }
    try {
      let rs: any = await this.authenTokenService.listTokenPassward(info);

      if (rs.statusCode == 200) {
        sessionStorage.setItem('authentoken', rs.info);
        this.token = sessionStorage.getItem('authentoken');
        console.log('regenerate token success');
      } else {
        this.alertService.error(rs.text, 'เกิดข้อผิดพลาดในการ regenerate token');
        console.log('regenerate token error');
      }
    } catch (error: any) {
      this.alertService.error(error, 'เกิดข้อผิดพลาดในการ regenerate token');
      // console.log(error.error);
    }
  }

}
