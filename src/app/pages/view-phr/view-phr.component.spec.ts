import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPhrComponent } from './view-phr.component';

describe('ViewPhrComponent', () => {
  let component: ViewPhrComponent;
  let fixture: ComponentFixture<ViewPhrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewPhrComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewPhrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
