import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewClaimDtComponent } from './view-claim-dt.component';

describe('ViewClaimDtComponent', () => {
  let component: ViewClaimDtComponent;
  let fixture: ComponentFixture<ViewClaimDtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewClaimDtComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewClaimDtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
