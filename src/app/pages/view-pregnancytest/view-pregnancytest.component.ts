import { Component, OnInit } from '@angular/core';
import { VisitService } from '../../shared/services-api/visit.service'
import { ClaimdmhtService } from '../../shared/services-api/claimdmht.service'
import { AlertService } from '../../shared/alert.service'
import * as moment from 'moment-timezone';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-pregnancytest',
  templateUrl: './view-pregnancytest.component.html',
  styleUrls: ['./view-pregnancytest.component.scss']
})
export class ViewPregnancytestComponent {
  toDate:any = Date();
  dataStart:any;
  dateEnd:any;  
  viewItem:any = [];
  loading:boolean = false;
  loadingTable:boolean =false;

  constructor(
    private visitService:VisitService,
    private claimdmhtService:ClaimdmhtService,
    private alertService:AlertService,
    private router: Router,
  ) {
    if(!sessionStorage.getItem('dataStart') && !sessionStorage.getItem('dateEnd')){
      sessionStorage.setItem('dataStart',moment(this.toDate).format('YYYY-MM-DD'));
      sessionStorage.setItem('dateEnd',moment(this.toDate).format('YYYY-MM-DD'));  
    }else{
      this.dataStart = sessionStorage.getItem('dataStart');
      this.dateEnd = sessionStorage.getItem('dateEnd');
    }
   }


   ngOnInit(): void {
    this.getInfo();
    if(!sessionStorage.getItem('authentoken')){
      this.router.navigate(["pages/viewtoken"]);
    }
    // if(!sessionStorage.getItem('dataStart') && !sessionStorage.getItem('dateEnd')){
    //   sessionStorage.setItem('dataStart',this.dataStart);
    //   sessionStorage.setItem('dateEnd',this.dateEnd);  
    // }
  }

  async getSearcha(){
    sessionStorage.setItem('dataStart',this.dataStart);
    sessionStorage.setItem('dateEnd',this.dateEnd);

    this.getInfo();
  }

  async getCancel(){
    this.dataStart = moment(this.toDate).format('YYYY-MM-DD');
    this.dateEnd = moment(this.toDate).format('YYYY-MM-DD');
    sessionStorage.setItem('dataStart',this.dataStart);
    sessionStorage.setItem('dateEnd',this.dateEnd);  

    this.getInfo();
  }

  async getInfo(){
    this.loadingTable = true
    let info:any = {
      "dateStart":sessionStorage.getItem('dataStart'),
      "dateEnd":sessionStorage.getItem('dateEnd')
  }
    try {
      let rs:any = await this.visitService.list_pregnancytest(info);
      console.log(rs);
      if(rs.statusCode == 200){
 
        this.viewItem = rs.results;
        this.loadingTable = false

      }else{
        this.loadingTable = false

        this.alertService.error(rs.text,'เกิดข้อผิดพลาด');
      }
    } catch (error:any) {
      console.log(error.error);
      this.loadingTable = false

      this.alertService.error(error.error.results.text,'เกิดข้อผิดพลาด');
    }
  }


}
