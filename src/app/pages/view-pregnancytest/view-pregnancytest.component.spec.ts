import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPregnancytestComponent } from './view-pregnancytest.component';

describe('ViewPregnancytestComponent', () => {
  let component: ViewPregnancytestComponent;
  let fixture: ComponentFixture<ViewPregnancytestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewPregnancytestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewPregnancytestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
