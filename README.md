# WebHIS for PHR

## Git Clone
    git clone https://gitlab.com/hi7-dev/web-app/web-hisforphr.git
    cd web-hisforphr

## Install

    npm i -g pm2 
    npm i --force 

## edit environment.ts และ environment.prod.ts

    apiUrl: 'http://ip-api:port',
    accessToken: "token-api"


## Edit server.js

    let port = 4200;


## Build

    npm run build

## Pm2 
    pm2 start server.js --name web-hisforphr 
